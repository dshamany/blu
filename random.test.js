const { num, hex, alp, calp, randomString, uuidV4, generateNum} = require('./random.module');

describe('random module tests', () => {
    test('generate one random number within bounds', () => {
        let random = Number(generateNum(hex));
        expect(random).toBeLessThanOrEqual(hex.length);
        expect(random).toBeGreaterThanOrEqual(0);
    });

    test('generate zero with no array', () => {
        let random = Number(generateNum());
        expect(random).toStrictEqual(0);
    });

    test('test constants', () => {
        expect(num).toBe('0123456789');
        expect(hex).toBe(num + 'abcdef');
        expect(alp).toBe('abcdefghjklmnopqrstuvwxyz');
        expect(calp).toBe('abcdefghjklmnopqrstuvwxyz'.toUpperCase());
    });

    test('generate random string of length 32', () => {
        let str = randomString(hex, 32);
        expect(str.length).toStrictEqual(32);
    });

    test('generate random 8-character string with default length using "0123456789abcdef" as input', () => {
        let str = randomString([...num, ...alp, ...calp]);
        expect(str.length).toStrictEqual(8);
    });

    test('generate random 16-character string with default values', () => {
        let str = randomString();
        expect(str.length).toStrictEqual(8);
        for (let c of str){
           expect(hex.includes(c)).toBeTruthy();
        }
    });

    test('generate uuid version 4', () => {
        let val = uuidV4();
        expect(val[8]).toStrictEqual('-');
        expect(val[13]).toStrictEqual('-');
        expect(val[14]).toStrictEqual('4');
        expect(val[18]).toStrictEqual('-');
        expect(val[23]).toStrictEqual('-');
    });

});