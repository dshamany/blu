const {randomString, hex} = require('./random.module');

class Document {
    constructor (obj){
        this.id = randomString(hex, 16);
        this.versions = [];
        this.created = new Date();
        this.modified = new Date();
        this.versions.push(obj);
        this.latest = this.versions[this.versions.length - 1];
    }
    versionCount = () => { return this.versions.length };
    getVersion = (v = this.versions.length - 1) => {
        let _v = Number(v);
        if (_v < 0 || _v >= this.versions.length && this.versions.length){
            return this.latest;
        }
        return this.versions[_v];
    }
    restoreVersion = (v) => {
        this.addVersion(this.getVersion(v));
    }
    addVersion = (slice) => {this.versions
        let _versions = this.versions;
        _versions.push(slice);
        this.versions = _versions;
        this.modified = new Date();
        return this.versions[this.versions.length - 1];
    }
    removeVersion = (index) => {
        if (index > this.versions.length || index < 0){
            return;
        }

        if (this.versions.length > 1){
            this.versions.splice(index, 1);
        }
        this.latest = this.versions[this.versions.length - 1];
    }
};

class DocumentList {
    constructor(){
        this.listObj = {};
    }
    create = (obj = {}) => {
        let document = new Document(obj);
        document.addVersion(obj);
        this.listObj[document.id] = document;
    }
    getById = (id) => {
        if (!this.listObj[id]){
            return;
        }            
        return this.listObj[id]
    }
    updateById = (id, newItem) => {
        this.listObj[id].addVersion(newItem);
    }
}

module.exports = {
    Document,
    DocumentList
}