const { Document, DocumentList } = require('./document.module');

describe('Document item tests', () => {
    test('Document is succeccfully created', () => {
        let document = new Document()
        expect(document).toBeTruthy();
    });
    
    test('Document is successfully created and has payload', () => {
        let document = new Document({test: 'success'});
        expect(document.latest).toBeTruthy();
    });
    
    test('Document is created successfully with verified payload', () => {
        let document = new Document({test: 'success'});
        expect(document.latest).toStrictEqual({test: 'success'});
    });
    
    test('get Document version count', () => {
        let document = new Document({test: 'success'});
        expect(document.versionCount()).toBe(1);
    });
    
    test('get Document version (default: latest)', () => {
        let document = new Document({test: 'success'});
        expect(document.getVersion()).toStrictEqual({test: 'success'});
    });

    test('get Document version 1 (zero-based)', () => {
        let document = new Document({test: 'success'});
        document.addVersion({test: 'success again'})
        expect(document.getVersion(1)).toStrictEqual({test: 'success again'});
    });

    test('successfully removed Document version 1', () => {
        let document = new Document({test: 'success'});
        document.addVersion({test: 'success again'})
        document.removeVersion(1)
        expect(document.getVersion(1)).toStrictEqual({test: 'success'});
    });

    test('does not remove version if it is the last one left', () => {
        let document = new Document({test: 'success'});
        document.addVersion({test: 'success again'})
        document.removeVersion(0);
        document.removeVersion(1);
        document.removeVersion(2);
        expect(document.getVersion()).toBeTruthy();
    });

    test('restore Document version 0', () => {
        let document = new Document({test: 'success'});
        document.addVersion({test: 'success again'})
        document.addVersion({test: 'success yet again'})
        document.restoreVersion(0);
        expect(document.getVersion(0)).toStrictEqual({test: 'success'});
    });
});

describe('Document list tests', () =>{
    test('Document list initilized', () => {
        let al = new DocumentList();
        expect(al).toBeTruthy();
        expect(al.create()).toBeFalsy();
    });
    
    test('Document list created item', () => {
        let al = new DocumentList();
        al.create({success: true});
        let keys = Object.keys(al.listObj).map(key => key);
        expect(keys.length).toBe(1);
    });

    test('Document list can find item by id', () => {
        let al = new DocumentList();
        al.create({success: true});
        let keys = Object.keys(al.listObj).map(key => key);
        let id = keys[0];
        expect(al.getById(id)).toBeTruthy();
    });

    test('Document list can return null when item not found', () => {
        let al = new DocumentList();
        expect(al.getById('1')).toBeFalsy();
    });

    test('Document list can update item by id', () => {
        let al = new DocumentList();
        al.create({success: true});
        let keys = Object.keys(al.listObj).map(key => key);
        let id = keys[0];
        al.updateById(id, {success: 'now string'});
        expect(al.getById(id).getVersion()).toStrictEqual({success: 'now string'});
    });
});