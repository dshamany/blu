const { Repeater } = require('./repeater.module');

describe('repeater tests', () => {

    test('repeater list initilized', () => {
        let repeater = new Repeater();
        expect(repeater).toBeTruthy();
    });

    test('repeater receives day name from day number', () => {
        let repeater = new Repeater();
        expect(repeater.getDayByNum(1)).toBe('monday');
    });

    test('repeater receives value array name from day number', () => {
        let repeater = new Repeater();
        repeater.subscribe('monday', '1234567890');
        expect(repeater.getValueByDayNum(1)[0]).toBe('1234567890');
    });

    test('repeater subscribes by day number', () => {
        let repeater = new Repeater();
        repeater.subscribeByDayNum(1, '1234567890')
        expect(repeater.getValueByDayNum(1)[0]).toBeTruthy();
    });

    test('repeater handles value when day is out of bounds ', () => {
        let repeater = new Repeater();
        expect(repeater.getDayByNum(7)).toBe('');
        expect(repeater.getDayByNum(-7)).toBe('');
    });
    
    test('repeater contains first item', () => {
        let repeater = new Repeater();
        repeater.subscribe('monday', '1234567890');
        expect(repeater.data['monday'][0]).toBe('1234567890');
    });

    test('repeater deletes first item successfully', () => {
        let repeater = new Repeater();
        repeater.subscribe('monday', '1234567890');
        repeater.unsubscribe('monday', '1234567890');
        expect(repeater.data['monday'].length).toBeFalsy();
    });

    test('repeater contains multiple items successfully', () => {
        let repeater = new Repeater();
        repeater.subscribe('monday', '1234567890');
        repeater.subscribe('monday', '0987654321');
        expect(repeater.data['monday'].length).toBe(2);
    });
    
    test('repeater displays all subscribed days from a single subscriber', () => {
        let repeater = new Repeater();
        repeater.subscribe('monday', '1234567890');
        repeater.subscribe('wednesday', '1234567890');
        expect(repeater.getBySubscriberId('1234567890')).toStrictEqual(['monday', 'wednesday']);
    });
});
