FROM node:latest

WORKDIR /src

COPY . .

RUN npm install --save-dev jest
RUN npm install

CMD [ "npm", "run", "test" ]