const { hex } = require('./random.module');

class Repeater {
    constructor (){
        this.data = {
            monday: [],
            tuesday: [],
            wednesday: [],
            thursday: [],
            friday: [],
            saturday: [],
            sunday: []
        }
        this.weekdays = ['', 
            'monday', 
            'tuesday', 
            'thursday', 
            'friday', 
            'saturday', 
            'sunday'
        ];

    }
    getBySubscriberId = (value) => {
        let _data = this.data;
        let keys = Object.keys(_data).map(day => day);
        let result = [];
        for (let key of keys){
            if (this.data[key].includes(value)) {
                result.push(key);
            }
        }
        return result;
    }
    getDayByNum(num){
        if (num > 0 && num < this.weekdays.length){
            return this.weekdays[num];
        }
        return '';
    }
    getValueByDayNum = (num) => {
        let day = this.getDayByNum(num);
        return this.data[day];
    }
    subscribe = (day, value) => {
        this.data[day].push(value);
    }
    subscribeByDayNum = (num, value) => {
        let day = this.weekdays[num];
        this.subscribe(day, value);
    }
    unsubscribe = (day, value) => {
        let _val = String(value);
        let _data = this.data;
        _data[day] = _data[day].filter((item) => item != _val);
        this.data = _data;
    }
};

module.exports = {
    Repeater,
}