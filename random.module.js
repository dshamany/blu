const num = '0123456789'
const hex = num + 'abcdef';
const alp = 'abcdefghjklmnopqrstuvwxyz';
const calp = alp.toUpperCase();

const generateNum = (arr) => {
    if (!arr){
        return 0;
    }
    return Number(Math.random() % arr.length * (arr.length - 1)).toFixed(0);
}

const randomString = (arr = hex, length = 8) => {
    let result = '';
    for (let i = 0; i < length; i++){
        let n = Number(generateNum(arr))
        result += arr[n];
    }
    return result;
}

const bag = alp + calp + num;

const uuidV4 = () => (
    randomString(bag, 8) + '-' + 
    randomString(bag,4) + '-4' + 
    randomString(bag, 3) + '-' + 
    randomString(bag, 4) + '-' + 
    randomString(bag, 12)
);

module.exports = {
    num, hex, alp, calp,
    generateNum,
    randomString,
    uuidV4,
}